[ There is an [English version](README-en.md) of this document]

# Petición de contribuciones para esLibre 2020

[ Por favor, reenvía esta petición de contribuciones a quien pueda encontrarla interesante ]

El [congreso esLibre](https://eslib.re) es un encuentro de personas interesadas en las tecnologías libres, enfocado a compartir conocimiento y experiencia alrededor de las mismas. La próxima edición se celebrará en la Universidad Rey Juan Carlos (Madrid) los días 5 y 6 de junio de 2020.

En esLibre tratamos de tener un congreso anual que recupere y mantenga la idea de comunidad, en el que podamos aprovechar la experiencia de las viejas glorias y las ganas de la gente nueva. Queremos conseguir un ambiente técnico, pero que no olvide, y explique, lo importante que es que la tecnología sea libre.

Queremos también abrir el congreso a la comunidad más amplia interesada en la cultura libre, quizás con menos enfoque en la tecnología, pero con igual interés en compartir.

## Temática

El software libre es la temática central de esLibre. Alrededor de ella, también se contemplan otras tecnologías libres (por ejemplo, hardware libre), y el mundo menos tecnológico del conocimiento libre. En líneas generales, temas relacionados directamente con obras (software, hardware, obras culturales, etc.) que se distribuyan con licencias libres, según la [definición de OSI de open source](https://opensource.org/osd) o la [definición de la FSF de software libre](https://www.gnu.org/philosophy/free-sw.html), o que cumplan la [definición de obra cultural libre](https://freedomdefined.org/Definition) se considera que encajan perfectamente en esLibre.

Aunque mucho software libre, y en general, obras libres, se distribuye comercialmente, queremos mantener esLibre como un foro neutro con respecto a los intereses comerciales. En general, no se aceptarán ponencias, talleres o salas que promocionen específicamente una oferta comercial.

## Tipos de contribuciones

Hay varios tipos de contribuciones que puedes proponer:

* Charla. Presentación "tradicional", en formato normal (25 min. aproximadamente) o en formato relámpago (10 min. aproximadamente). La organización podrá proponer cambios de formato a alguna de las charlas, por motivos organizativos o de contenidos. También podrá, de forma excepcional, proponer a alguna charla un formato más largo.

* Sala (devroom). Organización del programa de una sala, que discurrirá en paralelo con otras actividades del congreso. Normalmente una sala estará organizada por y/o para una comunidad, y habitualmente será especializada en un tema (aunque también puede ser generalista). Sólo como ejemplo, en 2019 tuvimos las siguientes salas: Software libre en la Universidad; Privacidad, descentralización y soberanía digital; Perl; Informática y Matemáticas.

* Taller. Presentación práctica, "manos en la masa". Puede ser en muchos formatos, desde demostraciones que los asistentes puedan seguir, hasta sesiones de iniciación a una tecnología, donde los asistentes puedan experimentar con ella.

* Otra. ¡Usa tu imaginación! Propón otros formatos, estamos interesados en explorar otras formas de compartir conocimiento.

En la plantilla para proponer cada tipo de contribución podrás encontrar algún detalle más.

## Fechas

* Fecha limite de propuesta de salas: 2 de marzo
* Fecha límite de propuesta de charlas, talleres y otras: 16 de marzo
* Fecha límite de programa de salas: 30 de abril
* Celebración del congreso EsLibre: 5 y 6 de junio

Todas las fechas son a las 23:59 hora de Madrid.

## Cómo proponer tu contribución

La petición de contribuciones está abierta a todo el mundo. La forma de proponer una contribución será mediante un [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) (pull request) en este repositorio.

Para cada tipo de contribución tienes una plantilla en la carpeta raíz de este repositorio. Cópiala a la carpeta del año en curso, dentro de la subcarpeta correspondiente (charlas, salas, etc.). Usa un nombre de fichero que no esté ya usado, y que refleje el título de tu propuesta. Rellénala y haz un merge request que la incluya, para proponer tu actividad.

Cualquiera podrá ver estas propuestas, y si quiere, comentarlas. La organización de esLibre irá comentando y aceptando (o no) las que le vaya pareciendo conveniente, de acuerdo con su idea sobre el congreso. Si propones una contribución, estate atento a los comentarios que puedas recibir, pueden incluir preguntas y peticiones de clarificación que sería conveniente responder.

Si tienes problemas de cualquier tipo, o no sabes cómo funciona la mecánica de abrir un merge request, [abre una incidencia en este repositorio](https://gitlab.com/eslibre/charlas/issues/new), por favor. Para poder hacerlo, tendrás que usar una cuenta de GitLab.
